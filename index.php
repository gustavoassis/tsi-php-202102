<?php
//BASICO DO BASICO

//MUITO UTIL PARA DEBUG (NUNCA USAR EM PRODUÇÃO)
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// 



//echo "Olá*Mundo!";

//------------------ BASICO
$nome = 'Gustavo Assis'; //uma variável em PHP

echo 'Olá,' . $nome .'! <br><br>'; //Aqui foi concatenado

echo " Olá, $nome ! ";

//-----------------CONDICIONAIS

if ($nome == 'Gustavo Assis') {

    echo '<br> <br> O nome é igual';

} else {

    echo '<br> <br> O nome não é igual! <br> ';
}

echo '<br><br>';

// SWITCH CASE


$dia = 'terça';

switch($dia) {
    case 'segunda':
        echo 'Estude!';
    break;

    case 'terça':
        echo 'Vá para aula de CMS!';
    break;

    case 'quarta':
        echo 'Vá ´para aula de BD';
    break;

    case 'quinta':
        echo 'Venha para cá e seja feliz';
    break;

    case 'sexta':
        echo 'Vá para o kombuka';
    break;

    default:

    echo 'Vá descansar';
    
}
echo '<br>';

// TERNÁRIO 

$animal = 'Cachorro';
$tipo = $animal == 'Cachorro' ? 'mamífero' : 'desconhecido';
echo"<br> $animal é um animal do tipo :  $tipo <br><br>";


$sobrenome_informado = 'Assis';
$sobrenome = $sobrenome_informado ?? 'não informado';
echo "Sobrenome : $sobrenome<br>";

// -------------------LOOPINGS

//FOR 
echo '<br> For :';
for ($i = 0; $i < 10; $i++){
    echo " <br> Linha $i";
}

echo '<br> <br> While : <br>';
// WHILE
$i = 0;

while ($i < 10) {

    echo "Linha $i<br>";

    $i++;
}
echo '<br><br> Do while : <br>';

// DO WHILE

    $i = 0;

    do{
        echo "Linha $i <br>";

        $i++;
    }while( $i < 10 );

echo '<br> 2 + 2 = ' . (2+2) . '!';

include 'link.html'; // se não existir link.html dá um erro mas continua a execução

require 'link.html'; // se não existir link.html, dá um erro fatal e para o programa

include_once 'link.html'; // verificar se já foi incluido antes, se sim, não inclui novamente

require_once 'link.html'; /// verificar se já foi incluido antes, se sim, não inclui novamente