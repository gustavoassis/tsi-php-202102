<?php

$despesas [0] = 345.55;
$despesas [1] = 135.00;
$despesas [2] = 600.00;
$despesas [3] = 900.00;
$despesas [4] = 400.00;

for ($i = 0; $i < 4 ; $i++) {
    echo $despesas[$i] . "<br>";
}


unset ($despesas); // Apaga ou destroi o objeto


$despesas ['Mercado'] = 345.55;
$despesas ['Estacionamento'] = 135.00;
$despesas['Alimentacao'] = 600.00;
$despesas['Bar'] = 900.00;
$despesas['Educacao'] = 400.00;

echo'<br> Despesas <br>';

foreach ( $despesas as $nome /*indice dentro []*/  => $gasto /*valor guardado*/ ) {
    echo "$nome : R$ " .number_format($gasto, 2, ',','.') ." <br>";
}


$rotina ['Segunda-feira'] = 'Academia , Trabalho, Senac';
$rotina ['Terça-feira'] = 'Academia , Trabalho, Senac';
$rotina ['Quarta-feira'] = 'Academia , Trabalho, Senac';
$rotina ['Quinta-feira'] = 'Academia , Trabalho, Senac';
$rotina ['Sexta-feira'] = 'Academia , Trabalho, Senac';
$rotina ['Sabado'] = 'Dormir';
$rotina ['Domingo'] = 'Descansar';

echo '<br> Minha Rotina <br>';
foreach ($rotina as $dia => $funcao) {
    echo "$dia: a minha rotina é :  $funcao <br>";
}
